package oopsassignment;
/* To Do
 * add takeTransport that calculates the fare
 * 	in that we need to ask for dest and source and calculate accordingly.
 */
import java.util.ArrayList;
// Static import allows us to import only functions instead of the whole library.
import static java.lang.Math.random;
import java.util.Scanner;

abstract class Transport{
	
	public String vehicle;
	public String emirate;
	public ArrayList<String> stopOvers;
	public double rate;
	public double fare;
	public double distTravelled;
	public int timeTaken;
	public String origin;
	public String destination;
	public String frequency;

	public void ride(String ori, String dest){
		origin = ori;
		destination = dest;
	}

	public void ride(){
		Scanner scan = new Scanner(System.in);

		System.out.println(stopOvers);
		System.out.println("Enter Source");
		origin = scan.nextLine();
		System.out.println("Enter Destination");
		destination = scan.nextLine();
	}

	public double calcFare(){
		double finalCost = rate*(stopOvers.indexOf(destination) - stopOvers.indexOf(origin));

		if (finalCost<0){finalCost = -finalCost;}

		return finalCost;

	}
}

class Metro extends Transport{

	public Metro(){

		vehicle = "Metro";
		stopOvers = new ArrayList<String>();
		rate = 1;
		fare = calcFare();
		frequency = "2 Mins";
	}	
	
	//public void addStopOvers(String stop){
	//	stopOvers.add(stop);
	//}
}

class Bus extends Transport{
	public Bus(){
		vehicle = "Bus";
		stopOvers = new ArrayList<String>();
		rate = 2.5;
		fare = calcFare();
		frequency = "20 Mins";
	}
	
	public void addStopOvers(String stop){
		stopOvers.add(stop);
	}
}

class Aeroplane extends Transport{
	public Aeroplane(){
		vehicle = "Aeroplane";
		stopOvers = new ArrayList<String>();
		rate = 200;
		fare = calcFare();
		frequency = null;
	}
	
	public void addStopOvers(String stop){
		stopOvers.add(stop);
	}

}

class Taxi extends Transport{
	public Taxi(){
		vehicle = "Taxi";
		stopOvers = new ArrayList<String>();
		rate = 1;
		fare = calcFare();
		frequency = "5 Mins";
	}
	public void addStopOvers(String stop){
		stopOvers.add(stop);
	}
}

class WaterWays extends Transport{
	public WaterWays(){
		vehicle = "Boat";
		stopOvers = new ArrayList<String>();
		rate = 2;
		fare = calcFare();
		frequency = "30 Mins";
	}
	public void addStopOvers(String stop){
		stopOvers.add(stop);
	}
}

class RentCar extends Transport{
	public RentCar(){
		vehicle = "Rent a car";
		stopOvers = new ArrayList<String>();
		rate = 1;
		fare = calcFare();
		frequency = null;
	}
	public void addStopOvers(String stop){
		stopOvers.add(stop);
	}
}

public class InitTransport{
	
	Metro DubaiMetro = new Metro();

	public InitTransport(){
		initMetro();
		initBus();

	}
	
	void initMetro(){
		System.out.println("success");
		// Metro init
		//Metro DubaiMetro = new Metro();
		DubaiMetro.emirate = "Dubai";
		DubaiMetro.stopOvers.add("UAE Excahnge");
		DubaiMetro.stopOvers.add("Danube");
		DubaiMetro.stopOvers.add("Energy");
		DubaiMetro.stopOvers.add("Ibn Batutua");
		DubaiMetro.stopOvers.add("Jabel Ali");
		DubaiMetro.stopOvers.add("DMCC");
		DubaiMetro.stopOvers.add("DAMAC");
		DubaiMetro.stopOvers.add("Nakheel");
		DubaiMetro.stopOvers.add("Dubai Internet City");
		DubaiMetro.stopOvers.add("Mashreq");
		DubaiMetro.stopOvers.add("MOE");
		DubaiMetro.stopOvers.add("First Abu Dhabi Bank");
		DubaiMetro.stopOvers.add("Noor Bank");
		DubaiMetro.stopOvers.add("Business Bay");
		DubaiMetro.stopOvers.add("Burj Khalifa/Dubai Mall");
		DubaiMetro.stopOvers.add("Financial Centre");
		DubaiMetro.stopOvers.add("Emirates Towers");
		DubaiMetro.stopOvers.add("WTC");
		DubaiMetro.stopOvers.add("Al jafiliya");
		DubaiMetro.stopOvers.add("");
		DubaiMetro.stopOvers.add("ADCB");
		DubaiMetro.stopOvers.add("Union");
		DubaiMetro.stopOvers.add("Al Raqqa");
		DubaiMetro.stopOvers.add("Deira City Centre");
		DubaiMetro.stopOvers.add("GGICO");
		DubaiMetro.stopOvers.add("Airport T1");
		DubaiMetro.stopOvers.add("Airport T2");
		DubaiMetro.stopOvers.add("Emirates");
		DubaiMetro.stopOvers.add("Rashidiya");

	}

	void initBus(){}
	
}

public class TransportChoice{

	void choice(){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("please choose Emirate");
		System.out.println("1. Abu Dhabi\n2. Dubai\n3. RasAlKhaimah\n4. Fujairah");
		int choice = scan.nextInt();
		

	}

	public static void main(String args[]){
		InitTransport initrans = new InitTransport();
		
	}
}
